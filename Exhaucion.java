package IA.Exhaucion;
//JDCH
public class Exhaucion {

    public static double aux;

    public static void Operaciones(int lados, double inicio) {
        int A = lados;
        double B = inicio, C, D, E, F, G, H, a, b, c, d, e;
        C = B / 2;
        a = Math.pow(C, 2);
        D = Math.sqrt(1 - a);
        E = 1 - D;
        b = Math.pow(E, 2);
        F = Math.sqrt(a + b);
        G = A * B;
        H = G / 2;

        aux = F;
        System.out.println("A " + A + "\n" + "B " + B + "\n" + "C " + C + "\n" + "D " + D + "\n" + "E " + E + "\n" + "F" + F + "\n" + "G " + G + "\n" + "H " + H);
        System.out.println("El numero PI es: " + H);
    }

    public static void main(String[] args) {
        int L = 6;
        double I = 1;

        for (int i = 0; i < 20; i++) {
            System.out.println("iteracion " + (1 + i));
            Operaciones(L, I);
            L = L * 2;
            I = aux;
            System.out.println("*******************************************");
        }
    }
}
